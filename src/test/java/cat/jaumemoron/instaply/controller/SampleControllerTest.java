package cat.jaumemoron.instaply.controller;

import cat.jaumemoron.instaply.InstaplyApplication;
import cat.jaumemoron.instaply.domain.Sample;
import cat.jaumemoron.instaply.service.SampleService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = InstaplyApplication.class)
@ExtendWith(SpringExtension.class)
public class SampleControllerTest {

    @InjectMocks
    private SampleController controller;

    @Mock
    private SampleService service;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @Test
    public void findByCustomer() {
        // Configure mocks
        var page = new PageImpl<Sample>(Collections.emptyList());
        when(service.findByCustomer(ArgumentMatchers.anyLong(), ArgumentMatchers.any(PageRequest.class))).thenReturn(page);
        // Execute method
        var response = controller.findByCustomer(1L, 0, 10);
        // Validate results
        assertNotNull(response);
        // Validate calls
        verify(service, times(1)).findByCustomer(ArgumentMatchers.anyLong(), ArgumentMatchers.any(PageRequest.class));
    }


    @Test
    public void create() {
        Long customerId = 1L;
        int degrees = 80;
        // Configure mocks
        var sample = Sample.of(customerId, degrees);
        sample.setId(1L);
        when(service.save(ArgumentMatchers.any(Sample.class))).thenReturn(sample);
        // Execute method
        var response = controller.create(customerId, degrees);
        // Validate results
        assertNotNull(response);
        assertEquals(customerId, response.getCustomerId());
        assertEquals(degrees, response.getDegrees());
        // Validate calls
        verify(service, times(1)).save(service.save(ArgumentMatchers.any(Sample.class)));
    }

}
