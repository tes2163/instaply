package cat.jaumemoron.instaply.controller;

import cat.jaumemoron.instaply.InstaplyApplication;
import cat.jaumemoron.instaply.domain.Alarm;
import cat.jaumemoron.instaply.service.AlarmService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = InstaplyApplication.class)
@ExtendWith(SpringExtension.class)
public class AlarmControllerTest {

    @InjectMocks
    private AlarmController controller;

    @Mock
    private AlarmService service;

    @Test
    public void findAll(){
        // Configure mocks
        var page = new PageImpl<Alarm>(Collections.emptyList());
        when(service.findAll(ArgumentMatchers.any(PageRequest.class))).thenReturn(page);
        // Execute method
        var response = controller.findAll(0, 10);
        // Validate results
        assertNotNull(response);
        // Validate calls
        verify(service, times(1)).findAll(ArgumentMatchers.any(PageRequest.class));
    }

    @Test
    public void findByCustomer(){
        // Configure mocks
        var page = new PageImpl<Alarm>(Collections.emptyList());
        when(service.findByCustomer(ArgumentMatchers.anyLong(), ArgumentMatchers.any(PageRequest.class))).thenReturn(page);
        // Execute method
        var response = controller.findByCustomer(1L,0, 10);
        // Validate results
        assertNotNull(response);
        // Validate calls
        verify(service, times(1)).findByCustomer(ArgumentMatchers.anyLong(), ArgumentMatchers.any(PageRequest.class));
    }

    @Test
    public void create(){
        Long customerId = 1L;
        String description  = "Description";
        int percent = 5;
        // Configure mocks
        var alarm = Alarm.of(customerId, description, percent);
        alarm.setId(1L);
        when(service.save(ArgumentMatchers.any(Alarm.class))).thenReturn(alarm);
        // Execute method
        var response = controller.create(customerId,description, percent);
        // Validate results
        assertNotNull(response);
        assertEquals(customerId, response.getCustomerId());
        assertEquals(description, response.getDescription());
        assertEquals(percent, response.getPercent());
        // Validate calls
        verify(service, times(1)).save(service.save(ArgumentMatchers.any(Alarm.class)));
    }

    @Test
    public void remove(){
        // Configure mocks
        doNothing().when(service).remove(ArgumentMatchers.anyLong());
        // Execute method
        controller.remove(1L);
        // Validate calls
        verify(service, times(1)).remove(ArgumentMatchers.anyLong());
    }
}
