package cat.jaumemoron.instaply.controller;

import cat.jaumemoron.instaply.InstaplyApplication;
import cat.jaumemoron.instaply.domain.Alert;
import cat.jaumemoron.instaply.domain.Sample;
import cat.jaumemoron.instaply.service.AlertService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = InstaplyApplication.class)
@ExtendWith(SpringExtension.class)
public class AlertControllerTest {

    @InjectMocks
    private AlertController controller;

    @Mock
    private AlertService service;

    @Test
    public void findAll() {
        // Configure mocks
        var page = new PageImpl<Alert>(Collections.emptyList());
        when(service.findAll(ArgumentMatchers.any(PageRequest.class))).thenReturn(page);
        // Execute method
        var response = controller.findAll( 0, 10);
        // Validate results
        assertNotNull(response);
        // Validate calls
        verify(service, times(1)).findAll(ArgumentMatchers.any(PageRequest.class));
    }

}
