package cat.jaumemoron.instaply.event;

import cat.jaumemoron.instaply.InstaplyApplication;
import cat.jaumemoron.instaply.domain.Alarm;
import cat.jaumemoron.instaply.domain.Alert;
import cat.jaumemoron.instaply.domain.Sample;
import cat.jaumemoron.instaply.service.AlarmService;
import cat.jaumemoron.instaply.service.AlertService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;

import static org.mockito.Mockito.*;

@SpringBootTest(classes = InstaplyApplication.class)
@ExtendWith(SpringExtension.class)
public class SampleEventListenerTests {

    @InjectMocks
    private SampleEventListener listener;

    @Mock
    private AlertService alertService;

    @Mock
    private AlarmService alarmService;

    @Test
    public void handle(){
        // Configure mocks
        when(alertService.save(ArgumentMatchers.any(Alert.class))).thenReturn(null);
        var alarm = Alarm.of(1L, "Alarm1", 5);
        alarm.setId(100L);
        var page = new PageImpl<Alarm>(Collections.singletonList(alarm));
        when(alarmService.findByCustomer(ArgumentMatchers.anyLong(), ArgumentMatchers.any(PageRequest.class))).thenReturn(page);
        // Testing sample 1: no alert should be created
        var event = SampleEvent.of(Sample.of(1L, 50));
        listener.handle(event);
        // Validate that nothing alert was created
        verify(alertService, times(0)).save(ArgumentMatchers.any(Alert.class));
        // Testing sample 2: no alert should be created
        event = SampleEvent.of(Sample.of(1L, 51));
        listener.handle(event);
        // Validate that nothing alert was created
        verify(alertService, times(0)).save(ArgumentMatchers.any(Alert.class));
        // Testing sample 2: no alert should be created
        event = SampleEvent.of(Sample.of(1L, 60));
        listener.handle(event);
        // Validate that nothing alert was created
        verify(alertService, times(1)).save(ArgumentMatchers.any(Alert.class));
    }
}
