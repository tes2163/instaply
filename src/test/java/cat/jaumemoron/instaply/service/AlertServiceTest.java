package cat.jaumemoron.instaply.service;

import cat.jaumemoron.instaply.InstaplyApplication;
import cat.jaumemoron.instaply.domain.Alarm;
import cat.jaumemoron.instaply.domain.Alert;
import cat.jaumemoron.instaply.domain.Sample;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = InstaplyApplication.class)
@ExtendWith(SpringExtension.class)
public class AlertServiceTest {

    @Autowired
    private AlarmService alarmService;

    @Autowired
    private SampleService sampleService;

    @Autowired
    private AlertService alertService;

    @Test
    public void create() {
        // Execute method
        var page = alertService.findAll(PageRequest.of(0, 2));
        // Validate results
        assertNotNull(page);
        assertTrue(page.getContent().isEmpty());
        // Create alert to test
        var alarm = Alarm.of(1L, "Description", 5);
        // Execute method
        alarm = alarmService.save(alarm);
        // Validate results
        assertNotNull(alarm);
        assertNotNull(alarm.getId());

        // Create sample to test
        var sample = Sample.of(1L, 80);
        // Execute method
        sample = sampleService.save(sample);
        // Validate results
        assertNotNull(sample);
        assertNotNull(sample.getId());
        assertEquals(1L, sample.getCustomerId());
        assertEquals(80, sample.getDegrees());

        // Create an alert
        var alert  = alertService.save(Alert.of(alarm, sample));
        // Validate results
        assertNotNull(alert);
        assertNotNull(alert.getAlarm());
        assertNotNull(alert.getSample());
        assertEquals(alarm.getId(), alert.getAlarm().getId());
        assertEquals(sample.getId(), alert.getSample().getId());
    }


}
