package cat.jaumemoron.instaply.service;

import cat.jaumemoron.instaply.InstaplyApplication;
import cat.jaumemoron.instaply.domain.Alarm;
import cat.jaumemoron.instaply.domain.Sample;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = InstaplyApplication.class)
@ExtendWith(SpringExtension.class)
public class SampleServiceTest {

    @Autowired
    private SampleService service;

    @Test
    public void allMethods() {
        // Execute method
        var page = service.findByCustomer(1L, PageRequest.of(0, 2));
        // Validate results
        assertNotNull(page);
        long size = page.getTotalElements();

        var sample = Sample.of(1L, 80);
        // Execute method
        sample = service.save(sample);
        // Validate results
        assertNotNull(sample);
        assertNotNull(sample.getId());
        assertEquals(1L, sample.getCustomerId());
        assertEquals(80, sample.getDegrees());

        // Execute method
        page = service.findByCustomer(1L, PageRequest.of(0, 2));
        // Validate results
        assertNotNull(page);
        assertFalse(page.getContent().isEmpty());
        assertEquals(size+1, page.getTotalElements());
    }


}
