package cat.jaumemoron.instaply.service;

import cat.jaumemoron.instaply.InstaplyApplication;
import cat.jaumemoron.instaply.domain.Alarm;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = InstaplyApplication.class)
@ExtendWith(SpringExtension.class)
public class AlarmServiceTest {

    @Autowired
    private AlarmService service;

    @Test
    public void allMethods() {
        // Execute method
        var page = service.findAll(PageRequest.of(0, 2));
        // Validate results
        assertNotNull(page);
        var size = page.getContent().size();

        var alarm = Alarm.of(1L, "Description", 5);
        // Execute method
        alarm = service.save(alarm);
        // Validate results
        assertNotNull(alarm);
        assertNotNull(alarm.getId());
        assertEquals(1L, alarm.getCustomerId());
        assertEquals("Description", alarm.getDescription());
        assertEquals(5, alarm.getPercent());

        // Execute method
        page = service.findAll(PageRequest.of(0, 2));
        // Validate results
        assertNotNull(page);
        assertEquals(size+1, page.getContent().size());

        // Execute method
        page = service.findByCustomer(1L, PageRequest.of(0, 2));
        // Validate results
        assertNotNull(page);
        assertFalse(page.getContent().isEmpty());
        assertEquals(size+1, page.getContent().size());

        // Execute method
        page = service.findByCustomer(2L, PageRequest.of(0, 2));
        // Validate results
        assertNotNull(page);
        assertTrue(page.getContent().isEmpty());

        // Execute method
        var alert2 = service.findById(alarm.getId());
        // Validate results
        assertTrue(alert2.isPresent());
        assertTrue(page.getContent().isEmpty());

        // Execute method
        service.remove(alarm.getId());
        // Validate results
        alert2 = service.findById(alarm.getId());
        assertFalse(alert2.isPresent());

    }


}
