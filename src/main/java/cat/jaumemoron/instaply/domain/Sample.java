package cat.jaumemoron.instaply.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@ApiModel(value = "Sample", description = "Samples sent by customers")
@Entity
@Table(name = "SAMPLES")
public class Sample {

    @SequenceGenerator(
            name = "SAMPLE_SEQUENCE_GENERATOR",
            sequenceName = "SAMPLES_SEQ",
            allocationSize = 1
    )
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SAMPLE_SEQUENCE_GENERATOR")
    @Column(name = "ID")
    @ApiModelProperty(value = "The unique id", required = true)
    private Long id;

    @Column(name = "CUSTOMER_ID")
    @ApiModelProperty(value = "The customer id", required = true)
    private Long customerId;

    @Column(name = "DEGREES")
    @ApiModelProperty(value = "The degress of the sample", required = true)
    private int degrees;

    @Column(name = "SAMPLE_DATE")
    @ApiModelProperty(value = "The sample date ", required = true)
    private LocalDateTime date = LocalDateTime.now();

    public Sample() {
    }

    private Sample(Long customerId, int degrees) {
        this.customerId = customerId;
        this.degrees = degrees;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public int getDegrees() {
        return degrees;
    }

    public void setDegrees(int degrees) {
        this.degrees = degrees;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public static Sample of(Long customerId, int degrees){
        return new Sample(customerId, degrees);
    }
}
