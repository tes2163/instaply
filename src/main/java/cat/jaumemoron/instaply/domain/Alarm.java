package cat.jaumemoron.instaply.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Objects;

@ApiModel(value = "Alarm", description = "Alarm configured by user")
@Entity
@Table(name = "ALARMS")
public class Alarm {

    @SequenceGenerator(
            name = "ALARM_SEQUENCE_GENERATOR",
            sequenceName = "ALARM_SEQ",
            allocationSize = 1
    )
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ALARM_SEQUENCE_GENERATOR")
    @Column(name = "ID")
    @ApiModelProperty(value = "The unique id", required = true)
    private Long id;

    @Column(name = "CUSTOMER_ID")
    @ApiModelProperty(value = "The customer id", required = true)
    private Long customerId;

    @Column(name = "DESCRIPTION")
    @ApiModelProperty(value = "The alarm description", required = true)
    private String description;
    @Column(name = "PERCENT")
    @ApiModelProperty(value = "If the new temperature readings is greater than this value, an alert will be created", required = true)
    private int percent;

    public Alarm() {
    }

    private Alarm(Long customerId, String description, int percent) {
        this.customerId = customerId;
        this.description = description;
        this.percent = percent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public static Alarm of(Long customerId, String description, int percent) {
        return new Alarm(customerId, description, percent);
    }
}
