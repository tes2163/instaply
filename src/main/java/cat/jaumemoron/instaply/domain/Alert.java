package cat.jaumemoron.instaply.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.time.LocalDateTime;

@ApiModel(value = "Alert", description = "Alerts created by system")
@Entity
@Table(name = "ALERTS")
public class Alert {

    @SequenceGenerator(
            name = "ALERT_SEQUENCE_GENERATOR",
            sequenceName = "ALERT_SEQ",
            allocationSize = 1
    )
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ALERT_SEQUENCE_GENERATOR")
    @Column(name = "ID")
    @ApiModelProperty(value = "The unique id", required = true)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ALARM_ID")
    @ApiModelProperty(value = "The alarm definition", required = true)
    private Alarm alarm;

    @ManyToOne
    @JoinColumn(name = "SAMPLE_ID")
    @ApiModelProperty(value = "The sample that create this alert", required = true)
    private Sample sample;

    @Column(name = "ALERT_DATE")
    @ApiModelProperty(value = "The alert creation date ", required = true)
    private LocalDateTime date = LocalDateTime.now();

    public Alert() {
    }

    private Alert(Alarm alarm, Sample sample) {
        this.alarm = alarm;
        this.sample = sample;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Alarm getAlarm() {
        return alarm;
    }

    public void setAlarm(Alarm alarm) {
        this.alarm = alarm;
    }

    public Sample getSample() {
        return sample;
    }

    public void setSample(Sample sample) {
        this.sample = sample;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public static Alert of(Alarm alarm, Sample sample){
        return new Alert(alarm, sample);
    }
}
