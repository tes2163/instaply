package cat.jaumemoron.instaply.controller;

import cat.jaumemoron.instaply.domain.Alarm;
import cat.jaumemoron.instaply.domain.Alert;
import cat.jaumemoron.instaply.domain.Sample;
import cat.jaumemoron.instaply.service.AlertService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static cat.jaumemoron.instaply.constants.RequestMappingConstants.*;

@RestController
@RequestMapping(API_REQUEST_MAPPING + ALERT_REQUEST_MAPPING)
@Api(tags = {"Alerts services"})
@Tag(name = "Alerts services", description = "Service to get alerts created by system")
public class AlertController {

    @Autowired
    private AlertService service;

    @GetMapping
    @ResponseStatus(value = HttpStatus.OK)
    @ApiOperation(value = "Get all alerts", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The alerts that belongs to a customer", response = Page.class)}
    )
    public Page<Alert> findAll(@ApiParam(value = "Zero-based element index", required = true)
                               @RequestParam(value = START_PARAM) Integer start,
                               @ApiParam(value = "The page size", required = true)
                               @RequestParam(value = SIZE_PARAM) Integer size) {
        return service.findAll(PageRequest.of(start, size, Sort.by(Sort.Direction.DESC, "date")));
    }

}
