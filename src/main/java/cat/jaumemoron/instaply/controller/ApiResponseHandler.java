package cat.jaumemoron.instaply.controller;

import cat.jaumemoron.instaply.dto.ApiError;
import cat.jaumemoron.instaply.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ApiResponseHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiError handle(IllegalArgumentException exception) {
        return ApiError.of(HttpStatus.BAD_REQUEST, exception.getLocalizedMessage());
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ApiError handle(NotFoundException exception) {
        return ApiError.of(HttpStatus.NOT_FOUND, exception.getLocalizedMessage());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ApiError handle(Exception exception) {
        return ApiError.of(HttpStatus.INTERNAL_SERVER_ERROR, exception.getLocalizedMessage());
    }
}
