package cat.jaumemoron.instaply.controller;

import cat.jaumemoron.instaply.domain.Alarm;
import cat.jaumemoron.instaply.exception.NotFoundException;
import cat.jaumemoron.instaply.service.AlarmService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static cat.jaumemoron.instaply.constants.RequestMappingConstants.*;

@RestController
@RequestMapping(API_REQUEST_MAPPING + ALARM_REQUEST_MAPPING)
@Api(tags = {"Alarm services"})
@Tag(name = "Alarm services", description = "Service to get configured alarms")
public class AlarmController {

    @Autowired
    private AlarmService service;

    @GetMapping
    @ResponseStatus(value = HttpStatus.OK)
    @ApiOperation(value = "Get all alarms", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The paginated list of all alarms", response = Page.class)}
    )
    public Page<Alarm> findAll(@ApiParam(value = "Zero-based element index", required = true)
                               @RequestParam(value = START_PARAM) Integer start,
                               @ApiParam(value = "The page size", required = true)
                               @RequestParam(value = SIZE_PARAM) Integer size) {
        return service.findAll(PageRequest.of(start, size));
    }

    @GetMapping(ID_MAPPING)
    @ResponseStatus(value = HttpStatus.OK)
    @ApiOperation(value = "Get an specific alarm", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The alarm data", response = Alarm.class)}
    )
    public Alarm findById(@ApiParam(value = "The alarm unique id", required = true)
                          @PathVariable(ID) Long id) {
        var retVal = service.findById(id);
        if (retVal.isPresent()) {
            return retVal.get();
        } else {
            throw new NotFoundException("Alarm does not exists");
        }
    }

    @GetMapping(CUSTOMER_REQUEST_MAPPING + ID_MAPPING)
    @ResponseStatus(value = HttpStatus.OK)
    @ApiOperation(value = "Get alarms of a customer", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The paginated alarms that belongs to a customer", response = Page.class)}
    )
    public Page<Alarm> findByCustomer(@ApiParam(value = "The customer owner of the alarm", required = true)
                                      @PathVariable(ID) Long customerId,
                                      @ApiParam(value = "Zero-based element index", required = true)
                                      @RequestParam(value = START_PARAM) Integer start,
                                      @ApiParam(value = "The page size", required = true)
                                      @RequestParam(value = SIZE_PARAM) Integer size) {
        return service.findByCustomer(customerId, PageRequest.of(start, size));
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    @ApiOperation(value = "Create a new alarm", response = Alarm.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "The alarm created", response = Alarm.class)}
    )
    public Alarm create(@ApiParam(value = "The customer owner of the alarm", required = true)
                        @RequestParam(value = CUSTOMER_ID) Long customerId,
                        @ApiParam(value = "The page size", required = true)
                        @RequestParam(value = ALARM_DESCRIPTION) String description,
                        @ApiParam(value = "The page size", required = true)
                        @RequestParam(value = ALARM_PERCENT) Integer percent) {
        return service.save(Alarm.of(customerId, description, percent));
    }

    @DeleteMapping(ID_MAPPING)
    @ResponseStatus(value = HttpStatus.OK)
    @ApiOperation(value = "Remove and existing alarm", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The alarm was removed successfully")}
    )
    public void remove(@ApiParam(value = "The alarm unique id", required = true)
                       @PathVariable(ID) Long id) {
        service.remove(id);
    }
}
