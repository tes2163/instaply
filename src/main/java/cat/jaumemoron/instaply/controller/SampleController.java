package cat.jaumemoron.instaply.controller;

import cat.jaumemoron.instaply.domain.Alarm;
import cat.jaumemoron.instaply.domain.Sample;
import cat.jaumemoron.instaply.event.SampleEvent;
import cat.jaumemoron.instaply.service.SampleService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static cat.jaumemoron.instaply.constants.RequestMappingConstants.*;

@RestController
@RequestMapping(API_REQUEST_MAPPING + SAMPLE_REQUEST_MAPPING)
@Api(tags = {"Samples services"})
@Tag(name = "Samples services", description = "Service to manage samples")
public class SampleController {

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    private SampleService service;

    @GetMapping(CUSTOMER_REQUEST_MAPPING + ID_MAPPING)
    @ResponseStatus(value = HttpStatus.OK)
    @ApiOperation(value = "Get samples of a customer", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The paginated alarms that belongs to a customer", response = Page.class)}
    )
    public Page<Sample> findByCustomer(@ApiParam(value = "The customer owner of the alarm", required = true)
                                       @PathVariable(ID) Long customerId,
                                       @ApiParam(value = "Zero-based element index", required = true)
                                       @RequestParam(value = START_PARAM) Integer start,
                                       @ApiParam(value = "The page size", required = true)
                                       @RequestParam(value = SIZE_PARAM) Integer size) {
        return service.findByCustomer(customerId, PageRequest.of(start, size, Sort.by(Sort.Direction.DESC, "date")));
    }


    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    @ApiOperation(value = "Create a new sample", response = Alarm.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "The sample created", response = Sample.class)}
    )
    public Sample create(@ApiParam(value = "The customer owner of the sample", required = true)
                         @RequestParam(value = CUSTOMER_ID) Long customerId,
                         @ApiParam(value = "The page size", required = true)
                         @RequestParam(value = SAMPLE_DEGREES) Integer degrees) {
        var sample = service.save(Sample.of(customerId, degrees));
        // Publish event
        applicationEventPublisher.publishEvent(SampleEvent.of(sample));
        return sample;
    }

 }
