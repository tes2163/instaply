package cat.jaumemoron.instaply.repository;

import cat.jaumemoron.instaply.domain.Sample;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SampleRepository extends PagingAndSortingRepository<Sample, Long> {

    Page<Sample> findByCustomerId(Long customerId, Pageable pageable);


}
