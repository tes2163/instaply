package cat.jaumemoron.instaply.repository;

import cat.jaumemoron.instaply.domain.Alert;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlertRepository extends PagingAndSortingRepository<Alert, Long> {


}
