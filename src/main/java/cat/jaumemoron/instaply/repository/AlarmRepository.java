package cat.jaumemoron.instaply.repository;

import cat.jaumemoron.instaply.domain.Alarm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlarmRepository extends PagingAndSortingRepository<Alarm, Long> {

    Page<Alarm> findByCustomerId(Long customerId, Pageable pageable);

}
