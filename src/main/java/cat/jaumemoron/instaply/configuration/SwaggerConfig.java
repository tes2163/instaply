package cat.jaumemoron.instaply.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${api.title}")
    private String apiTitle;

    @Value("${api.description}")
    private String apiDescription;

    @Value("${api.contact.name}")
    private String apiContactName;

    @Value("${api.contact.url}")
    private String apiContactUrl;

    @Value("${api.contact.email}")
    private String apiContactEmail;


    @Bean
    public Docket apiVersion1() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("cat.jaumemoron.instaply.controller"))
                .paths(PathSelectors.any())
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(new ApiInfo(
                        apiTitle,
                        apiDescription,
                        "1.0",
                        null,
                        new Contact(apiContactName, apiContactUrl, apiContactEmail),
                        null, null, Collections.emptyList()));
    }


}
