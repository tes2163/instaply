package cat.jaumemoron.instaply.event;

import cat.jaumemoron.instaply.domain.Sample;

public class SampleEvent {

    private final Sample sample;

    private SampleEvent(Sample sample) {
        this.sample = sample;
    }

    public Sample getSample() {
        return sample;
    }

    public static SampleEvent of (Sample sample){
        return new SampleEvent(sample);
    }
}
