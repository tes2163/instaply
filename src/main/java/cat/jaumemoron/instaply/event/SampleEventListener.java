package cat.jaumemoron.instaply.event;

import cat.jaumemoron.instaply.domain.Alarm;
import cat.jaumemoron.instaply.domain.Alert;
import cat.jaumemoron.instaply.service.AlarmService;
import cat.jaumemoron.instaply.service.AlertService;
import cat.jaumemoron.instaply.service.SampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class SampleEventListener {

    @Autowired
    private AlarmService alarmService;

    @Autowired
    private SampleService sampleService;

    @Autowired
    private AlertService alertService;

    private Map<Long, Integer> lastSamples = new HashMap<>();

    @EventListener
    public void handle(SampleEvent event) {
        Integer last = lastSamples.get(event.getSample().getCustomerId());
        Integer current = event.getSample().getDegrees();
        if (!ObjectUtils.isEmpty(last)){
            // Find for alarms defined by customer that should generate an alert
            var alarm = validate(event.getSample().getCustomerId(), last, current);
            if (!ObjectUtils.isEmpty(alarm)) {
                // Create alert
                alertService.save(Alert.of(alarm, event.getSample()));
            }
        }
        // Update last sample
        lastSamples.put(event.getSample().getCustomerId(), current);
    }

    public Alarm validate(Long customerId, int lastValue, int currentValue){
        var continueSearching = true;
        var currentPage = 0;
        Alarm candidate = null;
        do {
            var page = alarmService.findByCustomer(customerId, PageRequest.of(currentPage++, 10));
            for (var alarm : page.getContent()) {
                // The highest value is got
                int value = lastValue+(lastValue*alarm.getPercent()/100);
                if (currentValue > value) {
                    return alarm;
                }
            }
            if (currentPage >= page.getTotalPages()) {
                continueSearching = false;
            }
        } while (continueSearching);
        return candidate;
    }
}
