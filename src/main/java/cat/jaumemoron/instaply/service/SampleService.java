package cat.jaumemoron.instaply.service;

import cat.jaumemoron.instaply.domain.Sample;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SampleService {

    Page<Sample> findByCustomer(Long customerId, Pageable pageable);

    Sample save(Sample sample);
}
