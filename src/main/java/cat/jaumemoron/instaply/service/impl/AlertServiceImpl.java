package cat.jaumemoron.instaply.service.impl;

import cat.jaumemoron.instaply.domain.Alert;
import cat.jaumemoron.instaply.repository.AlertRepository;
import cat.jaumemoron.instaply.service.AlertService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import static cat.jaumemoron.instaply.constants.Errors.ALERT_IS_NULL;
import static cat.jaumemoron.instaply.constants.Errors.PAGEABLE_OBJECT_IS_NULL;

@Service
public class AlertServiceImpl implements AlertService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AlertServiceImpl.class);

    @Autowired
    private AlertRepository repository;


    @Override
    public Page<Alert> findAll(Pageable pageable) {
        if (ObjectUtils.isEmpty(pageable)) {
            throw new IllegalArgumentException(PAGEABLE_OBJECT_IS_NULL);
        }
        return repository.findAll(pageable);
    }

    @Override
    public Alert save(Alert alert) {
        if (ObjectUtils.isEmpty(alert)) {
            throw new IllegalArgumentException(ALERT_IS_NULL);
        }
        return repository.save(alert);
    }

}
