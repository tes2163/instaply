package cat.jaumemoron.instaply.service.impl;

import cat.jaumemoron.instaply.domain.Alarm;
import cat.jaumemoron.instaply.repository.AlarmRepository;
import cat.jaumemoron.instaply.service.AlarmService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

import static cat.jaumemoron.instaply.constants.Errors.*;

@Service
public class AlarmServiceImpl implements AlarmService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AlarmServiceImpl.class);

    @Autowired
    private AlarmRepository repository;

    @Override
    public Page<Alarm> findAll(Pageable pageable) {
        if (ObjectUtils.isEmpty(pageable)) {
            throw new IllegalArgumentException(PAGEABLE_OBJECT_IS_NULL);
        }
        return repository.findAll(pageable);
    }

    @Override
    public Optional<Alarm> findById(Long id) {
        if (ObjectUtils.isEmpty(id)) {
            throw new IllegalArgumentException(ALARM_ID_IS_NULL);
        }
        return repository.findById(id);
    }

    @Override
    public Page<Alarm> findByCustomer(Long customerId, Pageable pageable) {
        if (ObjectUtils.isEmpty(customerId)) {
            throw new IllegalArgumentException(CUSTOMER_ID_IS_NULL);
        }
        if (ObjectUtils.isEmpty(pageable)) {
            throw new IllegalArgumentException(PAGEABLE_OBJECT_IS_NULL);
        }
        return repository.findByCustomerId(customerId, pageable);
    }

    @Override
    public Alarm save(Alarm alarm) {
        if (ObjectUtils.isEmpty(alarm)){
            throw new IllegalArgumentException(PAGEABLE_OBJECT_IS_NULL);
        }
        return repository.save(alarm);
    }

    @Override
    public void remove(Long id) {
        if (ObjectUtils.isEmpty(id)) {
            throw new IllegalArgumentException(ALARM_ID_IS_NULL);
        }
        repository.deleteById(id);
    }
}
