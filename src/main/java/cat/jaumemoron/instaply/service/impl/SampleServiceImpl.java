package cat.jaumemoron.instaply.service.impl;

import cat.jaumemoron.instaply.domain.Sample;
import cat.jaumemoron.instaply.repository.SampleRepository;
import cat.jaumemoron.instaply.service.SampleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import static cat.jaumemoron.instaply.constants.Errors.CUSTOMER_ID_IS_NULL;
import static cat.jaumemoron.instaply.constants.Errors.PAGEABLE_OBJECT_IS_NULL;

@Service
public class SampleServiceImpl implements SampleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SampleServiceImpl.class);

    @Autowired
    private SampleRepository repository;


    @Override
    public Page<Sample> findByCustomer(Long customerId, Pageable pageable) {
        if (ObjectUtils.isEmpty(customerId)) {
            throw new IllegalArgumentException(CUSTOMER_ID_IS_NULL);
        }
        if (ObjectUtils.isEmpty(pageable)) {
            throw new IllegalArgumentException(PAGEABLE_OBJECT_IS_NULL);
        }
        return repository.findByCustomerId(customerId, pageable);
    }

    @Override
    public Sample save(Sample sample) {
        if (ObjectUtils.isEmpty(sample)) {
            throw new IllegalArgumentException(PAGEABLE_OBJECT_IS_NULL);
        }
        return repository.save(sample);
    }

}
