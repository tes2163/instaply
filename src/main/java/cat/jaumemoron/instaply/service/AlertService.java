package cat.jaumemoron.instaply.service;

import cat.jaumemoron.instaply.domain.Alert;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface AlertService {

    Page<Alert> findAll(Pageable pageable);

    Alert save(Alert alert);

}
