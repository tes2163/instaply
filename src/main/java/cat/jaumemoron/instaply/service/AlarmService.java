package cat.jaumemoron.instaply.service;

import cat.jaumemoron.instaply.domain.Alarm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface AlarmService {

    Page<Alarm> findAll(Pageable pageable);

    Optional<Alarm> findById(Long id);

    Page<Alarm> findByCustomer(Long customerId, Pageable pageable);

    Alarm save(Alarm alarm);

    void remove(Long id);
}
