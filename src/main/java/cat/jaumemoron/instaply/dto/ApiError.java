package cat.jaumemoron.instaply.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.http.HttpStatus;

@ApiModel(value = "Error", description = "Object with data about an error")
public class ApiError {

    @ApiModelProperty(value = "The timestamp when the response was sent", required = true, example = "1555572810405")
    private long timestamp = System.currentTimeMillis();
    @ApiModelProperty(value = "The Http code of the error", required = true)
    private HttpStatus status;
    @ApiModelProperty(value = "The error message", required = true)
    private String message;

    private ApiError(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static ApiError of(HttpStatus status, String message){
        return new ApiError(status, message);
    }
}
