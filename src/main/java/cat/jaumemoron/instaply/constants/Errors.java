package cat.jaumemoron.instaply.constants;

public class Errors {

    public static final String PAGEABLE_OBJECT_IS_NULL = "Pageable object cannot be null";

    public static final String ALARM_ID_IS_NULL = "Alarm id cannot be null";

    public static final String CUSTOMER_ID_IS_NULL = "Customer id cannot be null";

    public static final String ALERT_IS_NULL = "Alert object cannot be null";
}
