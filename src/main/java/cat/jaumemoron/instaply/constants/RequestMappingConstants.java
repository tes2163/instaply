package cat.jaumemoron.instaply.constants;

public class RequestMappingConstants {

    public static final String API_REQUEST_MAPPING = "/api";
    public static final String ALARM_REQUEST_MAPPING = "/alarm";

    public static final String SAMPLE_REQUEST_MAPPING = "/sample";

    public static final String ALERT_REQUEST_MAPPING = "/alert";

    public static final String CUSTOMER_REQUEST_MAPPING = "/customer";

    public static final String ENABLED_REQUEST_MAPPING = "/enabled";

    public static final String ID = "id";
    public static final String ID_MAPPING = "/{" + ID + "}";

    public static final String START_PARAM = "start";
    public static final String SIZE_PARAM = "size";

    public static final String CUSTOMER_ID = "customerId";

    public static final String ALARM_DESCRIPTION = "description";
    public static final String ALARM_PERCENT = "percent";

    public static final String SAMPLE_DEGREES = "degrees";
}
