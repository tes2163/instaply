# Instaply

## Descripción

Esta API permite la definición de alarmas para customers (que podrian ser maquinarias diferentes)

Cuando llega una nueva medición de temperatura de un customer, se evalúan todas las alarmas definidas. Cuando se encuentra que una alarma cumple la condición indicada se crea una alerta. No se evalúan el resto de alarmas definidas una vez una cumpla la condición.

Para ello, se ha definido una api con tres conjuntos de servicios:

1. Servicios para la gestión de alarmas
2. Servicios para la recepción de temperaturas
3. Servicios para obtener las alertas

## Ejecución

Para la ejecución del proyecto con cualquier editor:

1. Crear proyecto nuevo a través de la url del repositorio.
2. Añadir el directorio profiles/local como directorio de resources.

Desde consola:


Una vez tenemos el jar generado, en el mismo directorio crear directorio conf y copiar dentro el fichero profiles/local/application.yml.


Ejecutar el siguiente comando:

    java -Djava.security.egd=file:/dev/./urandom -jar app.jar --spring.config.location=file:/conf/


En ambos casos, una vez levantado el servicio poner en el navegador la url siguiente para ver la definición de Swagger.

    http://localhost:8080/swagger-ui/


## Pruebas

La aplicación levanta una Bdd en memoria con uns customers definidos (ids 1 y 2)

Para comprobar el funcionamiento, a parte de validar todos los servicios publicados por Swagger, se pueden realizar los siguientes pasos:

1) Definir una nueva alarma para el customer 1 indicando 5 como porcentaje.
2) Enviar los datos de una nueva temperatura (hasta ese momento no habrá ninguna)
3) Enviar los datos de una segunda temperatura. Si los grados superan en un 5% a la anterior se creará una alerta
4) Ir al servicio de alertas para ver la alerta creada


## Mejoras

Como es una prueba técnica de un tiempo limitado hay una serie de puntos que no se han tratado. Son los siguietnes:

### Autenticación & autorización

El acceso a la API debería estar securizado para que no cualquiera pudiera acceder. La autorizaciòn, además, impediría que cualquiera
pudiera ver las alertas y datos de otro 

### Gestión de customers

Los Customers no tienen ningún tipo de mantenimiento. Además, los servicios tampoco validan que cuando se crea una alerta/temperatura sean de un customer ya existente

### Definición de las alertas

La definición es muy rudimentaria: validar que una temperatura no sea mayor al % definido de la anterior temperatura.

### Gestión de eventos

La gestión de eventos es síncrona y está en la misma transacción que la escritura de la temperatura. Debería separarse ya que esto 
podría causar que si hay algún error en la gestión del evento, la escritura de la temperatura no se realizaría. Dos opciones:

1) Debería ser un tratamiento asíncrono y en otra transacción.
2) Que el evento se enviara a un sistema de mensajería (Apache MQ o Kafka) y un segundo componente evaluara la alerta.

Por otro lado, se está guardando el valor antiguo dentro de un Map. De haber muchos customers, causaría un incremento de memoria